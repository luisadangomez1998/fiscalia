<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  App\Models\Role;
class RoleTableSeeder extends Seeder
{
    public function run()
    {
        $role = new Role();
        $role->name = 'Administrador';
        $role->description = 'Administrador';
        $role->save();
        $role = new Role();
        $role->name = 'SuperUsuario';
        $role->description = 'SuperUsuario';
        $role->save();
        $role = new Role();
        $role->name = 'Supervisor';
        $role->description = 'Supervisor';
        $role->save();
    }
}
