<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        // Tabla Muchos a Muchos
        $role_Administrador = Role::where('name', 'Administrador')->first();
        $role_SuperUsuario = Role::where('name', 'SuperUsuario')->first();
        $role_Supervisor = Role::where('name', 'Supervisor')->first();

        // Usuario 1 Administrador
        $user = new User();
        $user->name = 'Administrador1';
        $user->email = 'Administrador1@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_Administrador);

        // Usuario 2 Administrador

         $user = new User();
        $user->name = 'Administrador2';
        $user->email = 'Administrador2@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_Administrador);

        // Usuario 3 Administrador
        $user = new User();
        $user->name = 'Administrador3';
        $user->email = 'Administrador3@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_Administrador);

        // Usuario 4 Administrador
        $user = new User();
        $user->name = 'Administrador4';
        $user->email = 'Administrador4@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_Administrador);

        // Usuario 5 Administrador
        $user = new User();
        $user->name = 'Administrador5';
        $user->email = 'Administrador5@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_Administrador);



        // Usuario 6 SuperUsuario
        $user = new User();
        $user->name = 'SuperUsuario1';
        $user->email = 'SuperUsuario1@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_SuperUsuario);

        // Usuario 7 SuperUsuario
        $user = new User();
        $user->name = 'SuperUsuario2';
        $user->email = 'SuperUsuario2@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_SuperUsuario);

        // Usuario 8 SuperUsuario
        $user = new User();
        $user->name = 'SuperUsuario3';
        $user->email = 'SuperUsuario3@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_SuperUsuario);

        // Usuario 9 SuperUsuario
        $user = new User();
        $user->name = 'SuperUsuario4';
        $user->email = 'SuperUsuario4@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_SuperUsuario);

        // Usuario 10 SuperUsuario
        $user = new User();
        $user->name = 'SuperUsuario5';
        $user->email = 'SuperUsuario5@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_SuperUsuario);

          // Usuario 11 Supervisor
          $user = new User();
          $user->name = 'Supervisor1';
          $user->email = 'Supervisor1@example.com';
          $user->password = bcrypt('12345');
          $user->save();
          $user->roles()->attach($role_Supervisor);

           // Usuario 12 Supervisor
           $user = new User();
           $user->name = 'Supervisor2';
           $user->email = 'Supervisor2@example.com';
           $user->password = bcrypt('12345');
           $user->save();
           $user->roles()->attach($role_Supervisor);

           // Usuario 13 Supervisor
           $user = new User();
           $user->name = 'Supervisor3';
           $user->email = 'Supervisor3@example.com';
           $user->password = bcrypt('12345');
           $user->save();
           $user->roles()->attach($role_Supervisor);

           // Usuario 14 Supervisor
           $user = new User();
           $user->name = 'Supervisor4';
           $user->email = 'Supervisor4@example.com';
           $user->password = bcrypt('12345');
           $user->save();
           $user->roles()->attach($role_Supervisor);

           // Usuario 15 Supervisor
           $user = new User();
           $user->name = 'Supervisor5';
           $user->email = 'Supervisor5@example.com';
           $user->password = bcrypt('12345');
           $user->save();
           $user->roles()->attach($role_Supervisor);

     }
}
