<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Role;
use Request;
use Hash;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $users = User::all();
    //     $roles = Role::all();
    //     return view('')
    //                                 ->with('users', $users)
    //                                 ->with('roles', $roles);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'roles' => Role::get()->pluck('name', 'id'),
        ];
        return view('create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function rol($num)
    {
        if ($num == 1) {
            return "Administrador";
        } elseif ($num == 2) {
            return "SuperUsuario";
        } else {
            return "Supervisor";
        }
    }


    public function store(Request $request)
    {

        $inputs = Request::all();

        $rules = [
            'email' => 'required|email|unique:users,email|max:100',

        ];
        $messages = [
            'email.unique' => 'Este correo ya esta en uso',
        ];
        $validar = Validator::make($inputs, $rules, $messages);
        if ($validar->fails()) {

            return Redirect::back()->withInput(Request::all())->withErrors($validar);
        } else {

        $inputs['password'] = Hash::make($inputs['password']);
        $name =  $this->rol($inputs['role_id']);
        $role_Administrador = Role::where('name',$name)->first();
        $user = User::create($inputs);
        $user->roles()->attach($role_Administrador);
        return Redirect::to('home');


        $inputs = Request::all();
        $name =  $this->rol($inputs['role_id']);
        $role = Role::where('name', $name)->first();
        $user = User::create($inputs);
        $user->roles()->attach($role);
        return Redirect::to('home');
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $data = [
            'user' => User::findOrFail($id),
            'roles' => Role::get()->pluck('name', 'id'),
        ];
        $data['user']->rol;
        return view('edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {




        $inputs = Request::all();

        $user = User::findOrFail($id);
        if ($inputs['password'] == '') {
            $datos_del_usuario = [
                'name' => $inputs['name'],
                'email' => $inputs['email'],


            ];
            $name =  $this->rol($inputs['role_id']);
            $role = Role::where('name', $name)->first();
            $user->roles()->sync($role);
            $user->fill($datos_del_usuario)->save();
        } else {

            $validated = request()->validate([
                'password'          => 'confirmed',
            ],
            [
                'confirmed'    => 'Las contraseñas no coinciden',
                'required'    => 'El campo es requerido',
            ] ,  [


            ]    );
            $inputs['password'] = Hash::make($inputs['password']);
            $datos_del_usuario = [
                'name' => $inputs['name'],
                'email' => $inputs['email'],
                'password' => $inputs['password'],

            ];
            $name =  $this->rol($inputs['role_id']);
            $role = Role::where('name', $name)->first();
            $user->roles()->sync($role);
            $user->fill($datos_del_usuario)->save();
        }
        return Redirect::to('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return Redirect::to('home');
    }
}
