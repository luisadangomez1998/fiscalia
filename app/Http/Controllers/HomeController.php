<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['SuperUsuario', 'Administrador','Supervisor']);
        $users = User::all();
        $roles = Role::all();
        return view('index')
                                    ->with('users', $users)
                                    ->with('roles', $roles);

    }


    public function config($id)
    {

        $data = [
            'user' => User::findOrFail($id),
            'roles' => Role::get()->pluck('name','id'),

        ];
        $data['user']->rol;
        return view('config')->with($data);
    }


}
