
@extends('layouts.app')

@section('content')

<div class="banner">
  <div class="container">
    <h1>Bienvenido a Test</h1>
    <p>El proyecto consta de los siguiente:</p>
    <ul>
<li>
<p style="text-align: justify;">El ejercicio se deber&aacute; resolver mediante el Framework de Laravel en su Versi&oacute;n 8.</p>
</li>
<li style="text-align: justify;">La Integraci&oacute;n con la Base de Datos deber&aacute; resolverse con SQL Server Versi&oacute;n 2017 o 2019.</li>
<li style="text-align: justify;">El CRUD deber&aacute; contener un Login de Acceso con 3 roles diferentes con las siguientes funcionalidades:
<ol>
<li>Administrador: Puede crear, editar y eliminar usuarios.</li>
<li>Super-Usuario: Solo puede crear nuevos usuarios.</li>
<li>Supervisor: solo puede listar los usuarios registrados.</li>
</ol>
</li>
<li style="text-align: justify;">La autentificaci&oacute;n deber&aacute; realizarse con laravel/ui o laravel/breeze.</li>
<li style="text-align: justify;">Se debe crear un filtro para los usuarios autenticados por medio de un middleware que permita acceder a las funcionalidades descritas en el punto n&uacute;mero 4.</li>
<li style="text-align: justify;">La base de datos deber&aacute; crearse por medio de MIGRACIONES DE LARAVEL con las relaciones y campos se&ntilde;alados en el diagrama 1.</li>
<li style="text-align: justify;">Las inserciones, consultas y/o modificaciones deber&aacute;n realizarse mediante los modelos en las bases de datos de laravel.</li>
<li style="text-align: justify;">La Tabla de usuario deber&aacute; llenarse con un seeder o factories de 15 usuarios, que permita generar usuarios de prueba aunado de las inserciones generadas por el CRUD.</li>
<li style="text-align: justify;">El formulario de registro y modificaci&oacute;n debe de contar un Request de validaci&oacute;n para todos los campos de acuerdo al tipo solicitado en la Base de Datos.</li>
</ul>

<br><br>
<p>Las contraseñas de los siguientes usuarios es "12345"</p>
<li style="text-align: justify;">Administrador1@example.com</li>
<li style="text-align: justify;">SuperUsuario1@example.com</li>
<li style="text-align: justify;">Supervisor1@example.com</li>


<br><br>
<p>¿Que se uso?</p>
<li style="text-align: justify;">Semantic en su ultima version</li>
<li style="text-align: justify;">Plugin Datatables</li>
<li style="text-align: justify;">yajra/laravel-datatables-oracle:"~9.0"</li>
<li style="text-align: justify;">Laravel Collective</li>
<li style="text-align: justify;">Laravel ui</li>
<li style="text-align: justify;">Laravel ui --Auth de Bootstrap</li>
<li style="text-align: justify;">SQLServer</li>



        @endsection
