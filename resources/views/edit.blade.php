@extends('layouts.app')

@section('content')
<div class="ui one column stackable  aligned page grid" style="margin-left: 30px;">
<div class="two   wide column">

              {{ Form::open( array('url' => 'update/'.$user->id, 'method' => 'GET', 'id' => '', 'class' => 'ui form', 'enctype' => 'multipart/form-data' ) ) }}




              <div class="field">
                    <label for="exampleInputEmail1">Correo Electronico</label>
                    <div class="ui disabled input">
                    <input  type="email" class="form-control " value="{{$user->email}}" name="email" id="email" placeholder="Correo electronico">
                </div>
                </div>


                <div class="field">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name" placeholder="Nombre completo del usuario">
                </div>




                <div class="field">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password" minlength="5"  id="Password" id="Password" placeholder="Contraseña">
                  </div>
                  <div class="field">
                        {{ Form::label ('role_id', 'Rol del usuario', ['class' => '']) }}
                        {!! Form::select('role_id', $roles ,$user->roles, ['class' => 'form-control']) !!}

                            <small class="form-text text-danger">{{ $errors->first('role_id') }}</small>

                    </div>

                <div class="field">
                    <button class="ui red right  icon button" data-tooltip="Cancelar modificaciones" data-position="bottom center"  href="{{URL::to('edit')}}">Cancelar</button>
                  <button type="submit" class="ui green right  icon button" data-tooltip="Guardar modificaciones del usuario" data-position="bottom center">Guardar</button>
                </div>
                </div>
             </div>
             {{ Form::close() }}
             </div>
             </div>
             @endsection


