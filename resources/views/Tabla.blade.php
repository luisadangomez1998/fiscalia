        <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.11.3/css/dataTables.semanticui.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.semanticui.css" />


        <div class="ui one column stackable center aligned page grid" style="margin-left: 30px;">
            <table id="example" class="ui celled table " style="width:100%; ">

                <thead>
                    <tr>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Correo Electronico</th>
                        <th class="text-center">Rol</th>
                        <th class="text-center">Creado</th>

                        @if(!!Auth::user()->hasRole('Administrador'))
                        <th class="text-center">Acciones</th>
                        @endif
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user)
                    @foreach ($user->roles as $role)
                    @if( $user->id == Auth::user()->id)

                    @else
                    <tr>
                        <td>
                            {{ $user->name }}
                        </td>
                        <td>{{ $user->email }}</td>
                        <td class="text-center">
                            @if ($role->name == 'Administrador')
                            <a class="ui olive label"> {{ $role->name }}</a>
                            @elseif ($role->name == 'SuperUsuario')
                            <a class="ui violet label"> {{ $role->name }}</a>
                            @else
                            <a class="ui teal label"> {{ $role->name }}</a>
                            @endif
                        <td class="text-center">{{ $user->created_at->format('d/m/Y H:i') }}</td>

                        @if(!!Auth::user()->hasRole('Administrador'))
                        <td class="text-center">
                            <div class="ui buttons">
                                <a class="ui positive button" href="{{URL::to('edit/'.$user->id)}}">Editar</a>

                                <div class="or" data-text="O"></div>
                                <a class="ui negative button" href="{{URL::to('destroy/'.$user->id)}}">Eliminar</a>

                            </div>
                        </td>
                        @endif
                    </tr>
                    @endif

                    @endforeach

                    @endforeach
                </tbody>

            </table>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/dataTables.semanticui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.semanticui.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#example').dataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    },

                });
            });
        </script>
