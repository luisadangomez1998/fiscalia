<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Test') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- semantic -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">


</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}"  data-tooltip="App test logo" data-position="bottom center">
                    <i class="quinscape blue icon"></i>

                </a>
                @if(Request::route()->getName() == 'home')

                @if(Auth::user()->hasRole('SuperUsuario') or Auth::user()->hasRole('Administrador'))
                <a class="ui primary icon button" data-tooltip="Añadir un nuevo usuario" data-position="bottom center" href="{{URL::to('create')}}">
                <i class="plus icon"></i>
                </a>
                @endif
                @endif



                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar sesion') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                                </li>
                            @endif
                        @else

                        @if(Request::route()->getName() == 'home')
                             <a class="ui inverted active primary right labeled icon button"  data-tooltip="Administrar Usuarios" data-position="bottom center" href="{{ route('home') }}">
                            <i class="user icon"></i> Usuarios</a>
                        @else
                            <a class="ui inverted primary right labeled icon button"  data-tooltip="Administrar Usuarios" data-position="bottom center" href="{{ route('home') }}">
                            <i class="user icon"></i> Usuarios </a>
                        @endif
                                </div>
                            </li>
                            <div class="ui compact  small menu">
                            <div class="ui simple small dropdown item">
                                {{Auth::user()->name}}
                                <i class="dropdown icon"></i>
                                <div class="menu">

                                <div class="item"  data-tooltip="Configuraciones de la cuenta" data-position="bottom center"
                                       onclick="event.preventDefault();
                                                     document.getElementById('config').submit();"> Configuraciones
                                </div>


                                <div class="item"  data-tooltip="Salir de la Sesion" data-position="bottom center"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> Cerrar sesion</div>
                            </div>
                            </div>
                            <form id="config" action="{{URL::to('config/'.Auth::user()->id)}}"  class="d-none">

                            @csrf
                                    </form>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <br><br>
            @yield('content')
        </main>

    </div>
</body>
</html>
