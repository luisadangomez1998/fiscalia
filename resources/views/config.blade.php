@extends('layouts.app')

@section('content')
<div class="ui one column stackable  aligned page grid" style="margin-left: 30px;">
    <div class="two   wide column">

        {{ Form::open( array('url' => 'update/'.$user->id, 'method' => 'GET', 'id' => '', 'class' => 'ui form', 'enctype' => 'multipart/form-data' ) ) }}

        <div class="ui items">
            <div class="item">
                <div class="image">
                    <div class="ui top blue  text-center attached label">{{$roles[1]}}</div>
                    <br>
                    <br>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJ6-IWIzvvi3l0tWgoqbtbQN6gukza_fYICg&usqp=CAU">
                </div>
                <div class="content">
                    <div class="field">
                        <label for="exampleInputEmail1">Correo Electronico</label>
                        <div class="ui disabled input">
                            <input type="email" class="form-control " value="{{$user->email}}" name="email" id="email" placeholder="Correo electronico">
                        </div>
                    </div>
                    <div class="field">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name" placeholder="Nombre completo del usuario">
                    </div>
                    <div class="description">
                        <p></p>
                    </div>
                    <div class="field">
                        <label for="exampleInputPassword1">Contraseña</label>
                        <input type="password" class="form-control" name="password" minlength="5"  id="Password" placeholder="Contraseña"  id="password">
                    </div>

                    <div class="field">
                        <label for="exampleInputPassword1">Confirma la contraseña</label>
                        <input type="password" class="form-control"  minlength="5"  name="password_confirmation" id="password_confirmation" placeholder="Confirma la contraseña">
                    </div>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    <div class="field disabled">
                        {{ Form::label ('role_id', 'Rol del usuario', ['class' => '']) }}
                        {!! Form::select('role_id', $roles ,$user->roles, ['class' => 'form-control']) !!}

                        <small class="form-text text-danger">{{ $errors->first('role_id') }}</small>

                    </div>
                    <div class="field">
                        <button class="ui red right  icon button" data-tooltip="Cancelar modificaciones" data-position="bottom center" href="{{URL::to('edit')}}">Cancelar</button>
                        <button type="submit" class="ui green right  icon button" data-tooltip="Guardar modificaciones del usuario" data-position="bottom center">Guardar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
{{ Form::close() }}


@endsection
