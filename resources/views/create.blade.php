@extends('layouts.app')

@section('content')
<div class="ui one column stackable  aligned page grid" style="margin-left: 30px;">
<div class="two   wide column">


              {{ Form::open( array('url' => 'store', 'method' => 'POST', 'id' => '', 'class' => 'ui.form form', 'enctype' => 'multipart/form-data' ) ) }}
              <div class="field">

              <div class="field">

                <label for="exampleInputEmail1">Correo Electronico</label>
                <input type="email" class="form-control "  name="email" required id="email" placeholder="Correo electronico" autocomplete="off">
                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                </div>
                <br>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" class="form-control " required name="name" required id="name" placeholder="Nombre completo del usuario">
                  </div>



                  <div class="field">

                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password" required id="Password" placeholder="Contraseña">
                  </div>
                  <br>

                  <div class="field">

                        {{ Form::label ('role_id', 'Rol del usuario', ['class' => 'required']) }}
                        {!! Form::select('role_id', $roles , null, ['class' => 'form-control']) !!}

                            <small class="form-text text-danger">{{ $errors->first('role_id') }}</small>

                    </div>
<br>
                    <div class="field">
                    <a  type="button" class="ui red right  icon button" data-tooltip="Cancelar modificaciones" data-position="bottom center"  href="{{URL::to('home')}}">Cancelar</a>
                  <button type="submit" class="ui green right  icon button" data-tooltip="Guardar modificaciones del usuario" data-position="bottom center">Guardar</button>
                </div>
                </div>


                {{ Form::close() }}
                </div>
             </div>
            @endsection
<script>

    $('form').form({
        on: 'blur',
        keyboardShortcuts: false,
        fields: {
            empty: {
                identifier: 'name',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a value'
                }]
            },
        }
    });
</script>


