## Test Direccion de Tecnologias la Informacion 

Hola buenas tardes a todos, soy Luis Adán Gómez Castro 

Les presento mi proyecto llamado "Test

## Sobre el Proyecto

Se deberá realizar un CRUD para Altas, Bajas y Modificaciones de Usuarios, el cual tendrá las siguientes restricciones:

- El ejercicio se deberá resolver mediante el Framework de Laravel en su Versión 8.
- La Integración con la Base de Datos deberá resolverse con SQL Server Versión 2017 o 2019.
- El tiempo límite de entrega será el viernes 20 de agosto a las 12:00 PM
- El CRUD deberá contener un Login de Acceso con 3 roles diferentes con las siguientes funcionalidades: 
	- 	Administrador: Puede crear, editar y eliminar usuarios.
	- 	Super-Usuario: Solo puede crear nuevos usuarios.
 	-	Supervisor: solo puede listar los usuarios registrados.
- La autentificación deberá realizarse con laravel/ui o laravel/breeze.
- Se debe crear un filtro para los usuarios autenticados por medio de un middleware que permita acceder a las funcionalidades descritas en el punto número 4.
- La base de datos deberá crearse por medio de MIGRACIONES DE LARAVEL con las relaciones y campos señalados en el diagrama 1.
- Las inserciones, consultas y/o modificaciones deberán realizarse mediante los modelos en las bases de datos de laravel. 
- La Tabla de usuario deberá llenarse con un seeder o factories de 15 usuarios, que permita generar usuarios de prueba aunado de las inserciones generadas por el CRUD.
- El formulario de registro y modificación debe de contar un Request de validación para todos los campos de acuerdo al tipo solicitado en la Base de Datos.


## Usuarios

Las contraseñas de los siguientes usuarios es "12345"

- Administrador1@example.com
- SuperUsuario1@example.com
- Supervisor1@example.com

## ¿Que se uso?

- Semantic en su ultima version
- Plugin Datatables
- yajra/laravel-datatables-oracle:"~9.0"
- Laravel Collective
- Laravel ui
- Laravel ui --Auth de Bootstrap
- SQLServer / SQLExpress
- Git

## ¿Como instalar?

1. Terminal **` git clone https://luisadangomez1998@bitbucket.org/luisadangomez1998/fiscalia.git` **
2. Terminal **` cd fiscalia` **
3. Terminal **` composer install` **
4. Terminal **` npm install` **
5. Configurar el archivo **.env** con la conexion correspondiente a **SQL server**
6. Terminal **` php artisan key:generate` **
7. Terminal **` php artisan migrate` **
	- Opcional **` php artisan migrate:refresh --seed` **
8. Acceder en tu navegador de preferencia a la siguiente ruta **` www.localhost.com:8000` **