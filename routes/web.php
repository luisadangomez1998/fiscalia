<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\usersController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/config/{id}', [HomeController::class, 'config'])->name('config');
// Usuarios
Route::get('create', [usersController::class, 'create']);
Route::POST('/store', [usersController::class, 'store']);
Route::get('edit/{id}', [usersController::class, 'edit']);
Route::GET('/update/{id}', [usersController::class, 'update']);
Route::GET('/destroy/{id}', [usersController::class, 'destroy']);



// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
